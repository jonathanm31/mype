@extends('index')
@section('contenido')
<?PHP
    header("Access-Control-Allow-Origin:*");
 ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">
<div class="header">
<div class="page-bar align-right" style="background-color: #fff; margin-bottom: 10px;">
          <ul class="page-breadcrumb">
              <li>
                  <a href="index.html">Panel</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Lista de Productos</span>
              </li>
          </ul>
      </div>
      <div class="navbar navbar-default" role="navigation">
      <ul class="horizontal">
          <li class="item"><a href="new_product"><i class="fa fa-book"></i>Nuevo</a></li>
          <li class="item"><a href="/"><i class="fa fa-angle-double-left"></i>Regresar</a></li>
        </ul>
      </div>
    </div>


<table class="display didden" id="products_table">
	<h2>Productos</h2>
	<thead>
		<tr>
      <th>Estado</th>
      <th>Nombre</th>
			<th>Categoria</th>
      <th>Precio Unitario</th>
      <th>Cantidad Total</th>			
      <th>Acciones</th>
		</tr>
	</thead>
	<tbody id="products">
		
	</tbody>
</table>


<div class="progress"></div>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<script src="{{ URL::asset('/javascript/products.js') }}" type="text/javascript"></script>

 @stop
