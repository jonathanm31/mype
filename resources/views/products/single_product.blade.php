@extends('index')
@section('contenido')
<?PHP
    header("Access-Control-Allow-Origin:*");
 ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">
<div class="header">
<div class="page-bar align-right" style="background-color: #fff; margin-bottom: 10px;">
          <ul class="page-breadcrumb">
              <li>
                  <a href="index.html">Panel</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Lista de Productos</span>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Ficha del producto</span>
              </li>
          </ul>
      </div>

   <div class="navbar navbar-default" role="navigation">
          <ul class="horizontal">
            <li class="item"><a id="guardar" data-id={{ $product->productID }}><i class="fa fa-save"></i>Guardar</a></li>              
              <li class="item"><a id="eliminar" data-id={{ $product->productID }}><i class="fa fa-close"></i>Eliminar</a></li>
              <li class="item"><a  href="ins?id={{ $product->productID }}"><i class="icon-arrow-up"></i>Nueva Entrada</a></li>
              <li class="item"><a href="outs?id={{ $product->productID }}"><i class="icon-arrow-down"></i></i>Nueva Salida</a></li>

              <li class="item"><a href="list_product"><i class="fa fa-angle-double-left"></i>Regresar</a></li>
           
         </ul>
  </div>
</div>      
<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-soft">
                                            <i class="icon-settings font-blue-soft"></i>
                                            <span class="caption-subject bold uppercase"> Producto: <b><small><?PHP echo strtoupper($product->name); ?></small></b></span>                                        </div>
                                       
                                    </div>
                                    <div class="portlet-body form">
                                            <div class="form-body">
                                            	<div class="form-group form-md-line-input">
                                               	  <label for="form_control_1">Categoria</label>
                                                    <input type="text" class="form-control" id="category" placeholder="Escoger categoria del lado izquierdo" value="{{ $category->description}}" disabled>                               
                                              </div>
                                              <div class="form-group form-md-line-input">
                                                   <label for="form_control_1">Nombre del Producto</label>
                                                    <input type="text" class="form-control"  id="name" placeholder="Nombre del Producto" value="{{$product->name}}">
                                              </div>
                                              <div class="form-group form-md-line-input has-success">
                                                	<label for="form_control_1">Cantidad Total</label>
                                                    <input type="number" class="form-control"  placeholder="Cantidad del producto" id="quantityTotal" value="{{$product->quantityTotal}}" disabled>  
                                              </div>
                                              <div class="form-group form-md-line-input has-success">                                                   <label for="form_control_1">Precio Unitario</label>
                                                    <input type="number" step="0.01" class="form-control"  value="{{$product->price}}" placeholder="000.00" id="price">   
                                              </div>
                                              <div class="form-group form-md-line-input has-info">
                                                   <label for="form_control_1">Estado</label>
                                                   <select id="state" class="form-control" >
                                                     <option value="0">Inactivo</option>
                                                     <option value="1">Activo</option>
                                                   </select>  
                                              </div>
                                            </div>
                                    </div>
                                </div>
                                 
    </div>
    <div class="col-md-8 col-sm-6 col-xs-12">
                  <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="icon-settings font-green"></i>
                                            <span class="caption-subject bold uppercase"> Transaciones Entradas y Salidas</span>                                        </div>
                                       
                                    </div>
                                    <div class="portlet-body form">
<table class="display didden" id="transacts_table">
  <thead>
    <tr>
      <th>Estado</th>
      <th>Tipo</th>
      <th>Cantidad</th>
      <th>Fecha</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody id="transacts">
    
  </tbody>
</table>                                    </div>
                                </div>

    </div>
</div>



<script type="text/javascript">
   var state = "<?php echo $product->state; ?>";
   $('#state').val(state);

</script>
<div class="progress"></div>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


<script src="{{ URL::asset('/javascript/singleProduct.js') }}" type="text/javascript"></script>

    @stop