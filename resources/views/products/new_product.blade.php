@extends('index')
@section('contenido')
<?PHP
    header("Access-Control-Allow-Origin:*");
 ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">
<div class="header">
<div class="page-bar align-right" style="background-color: #fff; margin-bottom: 10px;">
          <ul class="page-breadcrumb">
              <li>
                  <a href="index.html">Panel</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Lista de Productos</span>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Nuevo producto</span>
              </li>
          </ul>
      </div>
      <div class="navbar navbar-default" role="navigation">
      <ul class="horizontal">
            <li class="item"><a id="guardar"><i class="fa fa-save"></i>Guardar</a></li>
            <li class="item"><a id="regresar" href="list_product"><i class="fa fa-angle-double-left"></i>Regresar</a></li>
        </ul>
      </div>

    </div>
<div class="row">
    <div class="col-md-9 col-sm-7 col-xs-12 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-soft">
                                            <i class="icon-settings font-blue-soft"></i>
                                            <span class="caption-subject bold uppercase"> Datos del Producto</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                            	 <div class="form-group form-md-line-input">
                                                   <label for="form_control_1">Categoria</label disabled>
                                                   <input type="hidden" id="child" value="">
                                                    <select id="category" class="form-control">
                                                    <option value="0">-- --</option>
                                                      @foreach ($categorys as $category)
                                                      <option value="{{ $category->categID}}">{{$category->description}}</option>
                                                      @endforeach
                                                    </select>
                                                    
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                   <label for="form_control_1">Nombre del Producto</label>
                                                    <input type="text" class="form-control"  id="name" placeholder="Nombre del Producto">
                                                    
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                	<label for="form_control_1">Cantidad</label>
                                                    <input type="number" class="form-control"  placeholder="Cantidad del producto" id="quantityTotal">     
                                                    <span class="help-block font-red">*No es obligatorio</span>
                                                </div>
                                                <div class="form-group form-md-line-input has-success">
                                                   <label for="form_control_1">Precio Unitario</label>
                                                    <input type="number" step="0.01" class="form-control"  placeholder="000.00" id="price">
                                                   
                                                </div>
                                                
                                        </form>
                                    </div>
                                </div>
                                 
                            </div>
</div>

<script src="{{ URL::asset('/javascript/jstree.min.js') }}" type="text/javascript"></script>


<script src="{{ URL::asset('/javascript/newProduct.js') }}" type="text/javascript"></script>

    @stop