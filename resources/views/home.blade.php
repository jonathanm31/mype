@extends('panel')
@section('contenido')
<?PHP
    header("Access-Control-Allow-Origin:*");
 ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">

<div class="row">
                         <h1>Modulos</h1>
                         @if (Auth::user()->idrol == 1 || Auth::user()->idrol == 3 )
                            <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 purple" href="list_product">
                                    <div class="visual">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number"> +
                                            <span data-counter="counterup" data-value="89">5</span> </div>
                                        <div class="desc"> Usuarios </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red" href="list_product">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="12,5">125</span>/u </div>
                                        <div class="desc"> Productos </div>
                                    </div>
                                </a>
                            </div>
                            @endif
                         @if (Auth::user()->idrol == 0 || Auth::user()->idrol == 3 )
                          <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="caja">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="549">549</span>
                                        </div>
                                        <div class="desc"> Caja </div>
                                    </div>
                                </a>
                            </div>
                            @endif
                            
</div>
                

@stop