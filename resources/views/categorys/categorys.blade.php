@extends('index')
@section('contenido')
<?PHP
    header("Access-Control-Allow-Origin:*");
 ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">
<div class="header">
      <div class="page-bar align-right" style="background-color: #fff; margin-bottom: 10px;">
          <ul class="page-breadcrumb">
              <li>
                  <a href="index.html">Panel</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Lista de Categorias</span>
                  <i class="fa fa-angle-right"></i>
              </li>
          </ul>
      </div>
      <div class="navbar navbar-default" role="navigation">
      <ul class="horizontal">
          <li class="item"><a id="guardar"><i class="fa fa-save"></i>Guardar</a></li>
          <li class="item"><a id="eliminar"><i class="fa fa-close"></i>Eliminar</a></li>
          <li class="item"><a id="regresar" href="list_product"><i class="fa fa-angle-double-left"></i>Regresar</a></li>
        </ul>
      </div>

</div>
<div class="row">

<div class="col-md-8 col-sm-7 col-xs-12 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-soft">
                                            <i class="icon-settings font-blue-soft"></i>
                                            <span class="caption-subject bold uppercase"> Datos Categoria</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                            	<div class="form-group form-md-line-input has-error">
                                               	  <label for="form_control_1">Descripcion</label>
                                                    <input type="text" class="form-control" id="description">  
                                                    <input type="hidden" id="categID">                                               
                                                </div>
                                                <div class="form-group form-md-line-input">
                                                   <label for="form_control_1">Padre / Antesesor</label disabled>
                                                   <input type="hidden" id="child" value="">
                                                    <select id="padre" class="form-control">
                                                    <option value="0">-- --</option>}
                                                    option
                                                      @foreach ($categorys as $category)
                                                      <option value="{{ $category->categID}}">{{$category->description}}</option>
                                                      @endforeach
                                                    </select>
                                                    
                                                </div>   
                                              </div>                                  
                                        </form>
                                    </div>
                                </div>

                                 
      </div>
 <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-bubble font-green-sharp"></i>
                                            <span class="caption-subject font-green-sharp bold uppercase">Categorias</span>
                                        </div>                                       
                                    </div>
                                    <div class="portlet-body">
                                        <div id="tree_2" class="tree-demo jstree jstree-2 jstree-default jstree-checkbox-selection" >
                                        </div>
                                    </div>
                                </div>
</div>

</div>

<script src="{{ URL::asset('/javascript/jstree.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/javascript/categorys.js') }}" type="text/javascript"></script>

    @stop