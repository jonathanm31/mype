@extends('index')
@section('contenido')
<?PHP
    header("Access-Control-Allow-Origin:*");
 ?>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> 
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /> 
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/lista.css') }}" />

<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="base_url" content="{{ URL::to('/') }}">
<div class="header">
<div class="page-bar align-right" style="background-color: #fff; margin-bottom: 10px;">
          <ul class="page-breadcrumb">
              <li>
                  <a href="/">Panel</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Caja </span>
                  <i class="fa fa-angle-right"></i>
              </li>
          </ul>
      </div>
      <div class="navbar navbar-default" role="navigation">
      <ul class="horizontal">
            <li class="item"><a id="guardar"><i class="fa fa-save"></i>Crear Boleta</a></li>
            <li class="item"><a id="regresar" href="list_product"><i class="fa fa-angle-double-left"></i>Regresar</a></li>
        </ul>
      </div>

 </div>

 <div class="col-md-5 col-xs-12 col-sm-4">

            <div class="portlet box green-seagreen">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>DATOS BOLETA</div>
                </div>
                <div class="portlet-body flip-scroll">
                <div class="form-group form-md-line-input">
                                                   <label for="form_control_1">Nombre del Cliente</label>
                                                    <input type="text" class="form-control"  id="clientData" placeholder="Nombre del Cliente">
                                                    
                </div>
                <h5>Buscar Producto:</h5>
                <div class="input-group">                  
                  <span class="input-group-addon">
                      <i class="fa fa-plus"></i>
                  </span>
                 
                  <input type="text" id="search_prod" class="form-control" placeholder="Nombre de producto">
               </div>
              <div id="search_results" class="list-group col-md-12">
              </div>  
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <tbody>
                        <tr>
                        <td>TIPO DE PAGO </td>
                            <td>
                               
                               <select name="pago" id ="pago">
                                 <option    value="3" selected="selected">Pago Efectivo</option> 
                                 <option    value="1">Visa</option>
                                 <option    value="2">Mastercard</option>
                               </select>
                            </td>
                            
                         </tr>
                        <tr>
                            <td>SUB - TOTAL </td>
                            <td style="width: 50%;" id="stotal_detalle_t"></td>
                            <input type="hidden" id="stotal_detalle_t_i" name="stotal_detalle_t_i" value="0">
                        </tr>
                        <tr>
                            <td>IGV 18%: </td>
                            <td style="width: 50%;" id="igv_t"></td>
                            <input type="hidden" id="igv_t_i" name="igv_t_i" value="0">
                        </tr>
                        <tr>
                            <td>TOTAL </td>
                            <td style="width: 50%;" id="total_detalle_t"></td>
                            <input type="hidden" id="total_detalle_t_i" name="total_detalle_t_i" value="0">
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

    </div>
<div class="col-md-7 col-xs-12 col-sm-8">
   
            <div class="portlet box yellow-casablanca">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>DETALLE BOLETA</div>
                </div>
                <div class="portlet-body flip-scroll">
                <div class="table-scrollable table-responsive">
                    <table class="table table-striped table-bordered table-advance table-hover " id="caja_detalle">
                        <thead>
                        <tr>
                             
                            <th>
                                <i class="fa fa-briefcase"></i> Producto </th>
                            <th >
                                <i class="fa fa-align-justify"></i> En Stock </th>
                            <th >
                                <i class="fa fa-align-justify"></i> Cantidad </th>
                            <th>
                                <i class="fa fa-dollar"></i> Precio/u </th>
                            <th>
                                <i class="fa fa-dollar"></i> Importe </th>

                            <th> Acciones </th>
                        </tr>
                        </thead>
                        <tbody id="caja_detalle">

                        <tfoot>

                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->

</div>
<!-- Modal -->
<div id="selection">
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Boleta Generada</h4>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-md-8" >
                
            </div>
            <div class="col-md-4">
             fecha: <span id="fecha"></span>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8">
              Cliente: <span id="cliente"></span><br><br>
              Pago: <span id="tipo"></span>
              <span id="img"></span>
            </div>
            <div class="col-md-4">
            Sub-Total : <span id="stotal"></span><br>
            IGV : <span id="igv"></span><br>
            Total : <span id="total"></span>
            </div>
            <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-striped table-advance table-hover ">
                <caption><h4>Detalle Boleta</h4></caption>
                <thead>
                  <tr>                            
                            <th>
                                <i class="fa fa-briefcase"></i> Producto </th>
                            <th >
                                <i class="fa fa-align-justify"></i> Cantidad </th>
                            <th>
                                <i class="fa fa-dollar"></i> Precio/u </th>
                            <th>
                                <i class="fa fa-dollar"></i> Importe </th>

                   </tr>
                </thead>
                <tbody id="conf-bol">
                </tbody>
              </table>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="ok" class="btn btn-primary">Listo!</button>
      </div>
    </div>

  </div>
</div>
</div>
<script src="{{ URL::asset('/javascript/caja.js') }}" type="text/javascript"></script>

@stop