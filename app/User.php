<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   // protected $table = 'Account';
   // protected $primaryKey = 'AccountID';
    protected $fillable = [
        'name', 'email' , 'password','accountID'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'actived',
    ];

     public function publicaciones()
    {
        return $this->hasMany('App\Models\Publicaciones','idusuario');
    }
}
