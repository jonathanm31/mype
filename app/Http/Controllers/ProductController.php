<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use JWTAuth;

use Dingo\Api\Routing\Helpers;
use App\Models\Transactions;
use App\Models\Product;
use App\Models\Category;
use DB;
use App\User;
use Auth;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = Auth::user()->accountID;

        $products = Product::where('accountID',$user)->get();

        $categorys = array();


        foreach ($products as $product) {
             $category = $product->category()->first();
             array_push($categorys,$category);
        }       
 
        return  json_encode(compact('products','categorys'));
    }

    public function create()
    {
    	return view('products/view_products');      
    }
    public function newProduct(){
        $user = Auth::user()->accountID;
        $categorys = DB::table('Category')
                    ->where('Category.accountID',$user)
                    ->get();
        return view('products/new_product')->with('categorys',$categorys) ;
    }
    public function singleProduct(Request $request){
        $user = Auth::user()->accountID;
        $id = $request['id'];
        $product = DB::table('product')->where('productID',$id)->first();
        $category = DB::table('category')->where('categID',$product->categID)->first();

        return view('products/single_product')->with('product',$product)->with('category',$category);
    }

    public function findProducts(Request $request){
        $user = Auth::user()->accountID;
        $nom = $request['nom'];
        $products = DB::table('product')->where('accountID','=',$user)->where('name','like','%'.$nom.'%')->where('quantityTotal','>',0)->get();
        return response()->json($products);

    }

    public function store(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/

        $user = Auth::user()->accountID;

        $name = $request['name'];
        $categID = $request['categID'];
        $quantityTotal = $request['quantityTotal'];
        $price = $request['price'];
        $state = $request['state'];
        $type = $request['type'];


        $product = new Product;
        $product->name = $name;
        $product->categID = $categID;
        $product->quantityTotal = $quantityTotal;
        $product->price = $price;
        $product->state = $state;
        $product->accountID = $user;

        $product->save();

        if($quantityTotal > 0){
            $productID = $product->productID;
            $quantity = $quantityTotal;

            $transacts = new Transactions;
            $transacts->productID = $productID;
            $transacts->quantity = $quantity;
            $transacts->state = 1;
            $transacts->type = $type;
            $transacts->accountID = $user;

            $transacts->save();
        }

        return response()->json(['created'], 201);

    }
    public function show(Request $request){
    	$id = $request['id'];
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->accountID;    
        try {
            $product = Product::where('accountID','=',$user)->where('productID','=',$id)->first();
            $category = $product->category()->first();
            $transactions = $product->transacts()->limit(2000)->get()->toArray();
            return  response()->json(compact('product','category','transactions'));
        } catch (Exception $e) {
           return response()->json([$e], 409);
        }
    }

    public function edit($id){
    	
    }
    public function update(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/

        $user = Auth::user()->accountID;
        $id = $request['id'];
        $name = $request['name'];
        $categID = $request['categID'];
        $quantityTotal = $request['quantityTotal'];
        $price = $request['price'];
        $state = $request['state'];

        $product = Product::find($id);
        $product->name = $name;
        $product->categID = $categID;
        $product->quantityTotal = $quantityTotal;
        $product->price = $price;
        $product->state = $state;
        $product->accountID = $user;

        $product->save();
        if($product->quantityTotal == 0 && $quantityTotal > 0 ){
            $productID = $product->productID;
            $quantity = $quantityTotal;
            $state = 1;

            $transacts = new Transactions;
            $transacts->productID = $productID;
            $transacts->quantity = $quantity;
            $transacts->state = $state;
            $transacts->accountID = $user;

            $transacts->save();
        }elseif ($product->quantityTotal ==  $quantityTotal) {
            
        }else {
            $transacts = Transactions::where('productID',$product->productID)->first();
            $transacts->quantity = $quantityTotal;
            $transacts->save();

            //older register

        }

        return response()->json(['accepted'], 202);
    }
    public function destroy(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $id = $request['id'];
        $product = Product::find($id);
        try {
        	$product->delete();
        	return response()->json(['accepted'], 202);
        } catch (Exception $e) {
        	return response()->json(['conflict'], 409);
        }

        
    }


   
}