<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;

use Dingo\Api\Routing\Helpers;
use App\Models\Category;
use App\User;
use Auth;
use DB;

class CategoryController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
        /*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->accountID;
        $categorys = DB::table('Category')
                    ->select('Category.description AS id','cate.description AS parent','Category.description AS text')
                    ->where('Category.accountID',$user)
                    ->leftJoin('Category as cate','category.parentID','=','cate.categID')
                    ->get();
        return response()->json($categorys);
    }

    public function create()
    {
        $user = Auth::user()->accountID;
        $categorys = DB::table('Category')
                    ->where('Category.accountID',$user)
                    ->get();
        return view('categorys/categorys')->with('categorys',$categorys) ;   
    }

    public function get_ID(Request $request){
        $descr = $request['description'];
        $user = Auth::user()->accountID;
        $category =DB::table('Category')->where('description','like','%'.$descr.'%')->where('accountID',$user)->first();
        return response()->json($category);
    }

    public function store(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->accountID;

        $descr = $request['description'];
        $parentID = $request['parent'];

        $category = new Category;
        $category->description = $descr;
        $category->parentID = $parentID;
        $category->accountID = $user;

        $category->save();

        return response()->json(['created'], 201);

    }
    public function show(Request $request){
    	$id = $request['id'];
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->accountID;

        $category = Category::where('accountID',$user)->where('categID',$id)->first();
      
        return  response()->json($category);
    }

    public function edit($id){
    	
    }
    public function update(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $user = Auth::user()->accountID;

        $id = $request['id'];
        $descr = $request['description'];
        $parentID = $request['parent'];

        $category = Category::find($id);
        $category->description = $descr;
         if($parentID != null){
                if(!(bool)$this->is_child($id,$parentID)){
                     $category->parentID = $parentID;
                }else{
                   return response()->json(['conflict'], 409); 
               }              
        }
        $category->accountID = $user;

        $category->save();

        return response()->json(['accepted'], 202);
    }
    private function is_child($idNodo, $idParent){
        if($idParent == null || $idParent == 0){
            return false;
        }
        $data = DB::table('Category')
                ->where('categID',$idParent)
                ->where('parentID',$idNodo)->first();
        return is_object($data);
    }
    public function destroy(Request $request){
    	/*$currentUser = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);*/
        $id = $request['id'];
        $category = Category::find($id);
        
        try {
        	$category->delete();
        	return response()->json(['accepted'], 202);
        } catch (Exception $e) {
        	return response()->json(['conflict'], 409);
        }

        
    }

   
}