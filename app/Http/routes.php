<?php

Route::group(['middleware' => 'cors'], function(){
    Route::post('Auth_token','ApiAuthController@UserAuth');
    Route::get('prueba','PruebaController@index');

   

});

Route::group(['middleware' => ['web']], function () {

    Route::get('home', 'HomeController@index');
    Route::get('/', 'HomeController@index');
   
    Route::get('login/', function () {
        return view('login');
    });

    Route::get('mail',function(){
    	dd(Config::get('mail'));    	
    });
    Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');




});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    //////////////////////////////Con LOGGIN///////////////////////////////
     //categorys resfull
    Route::get('categorys','CategoryController@index');
    Route::post('category_store','CategoryController@store');
    Route::get('category_get','CategoryController@show');
    Route::post('category_update','CategoryController@update');
    Route::post('category_delete','CategoryController@destroy');
    Route::get('categorysID','CategoryController@get_ID');

    Route::get('list_categorys','CategoryController@create');

    //product resfull
    Route::get('list_product','ProductController@create');
    Route::get('single_product','ProductController@singleProduct');
    Route::get('new_product','ProductController@newProduct');
    Route::get('products','ProductController@index');
    Route::post('product_store','ProductController@store');
    Route::get('product_get','ProductController@show');
    Route::post('product_update','ProductController@update');
    Route::post('product_delete','ProductController@destroy');
    Route::get('product_find','ProductController@findProducts');
    //transact resfull
    Route::get('transacts','TransactController@index');
    Route::post('transact_store','TransactController@store');
    Route::get('transact_get','TransactController@show');
    Route::put('transact_update','TransactController@update');
    Route::delete('transact_delete','TransactController@destroy');

    Route::get('ins','TransactController@create'); 
    Route::get('outs','TransactController@create');

    //caja
    Route::get('caja','CajaController@index');
    
});

