<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CajaH extends Model {
    protected $table = 'cajaH';
    protected $primaryKey = 'cajahID'; 
    public $timestamps = false;

    public function detalles(){
    	return $this->hasMany('App\Models\CajaD','cajahID');
    }

    public function user(){
    	return $this->belongsTo('App\User','accountID','id');
    }
}