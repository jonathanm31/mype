<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model {
    protected $table = 'Category';
    protected $primaryKey = 'categID'; 
    protected $fillable = ['description'];
    public $timestamps = false;

    public function parent(){
    	return $this->hasOne('App\Models\Category','parentID');
    }

    public function user(){
    	return $this->belongsTo('App\User','accountID','id');
    }
}