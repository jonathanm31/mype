<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Product extends Model {
    protected $table = 'Product';
    protected $primaryKey = 'productID'; 
    protected $fillable = ['name','categID','quantityTotal','price','state','accountID'];
    public $timestamps = false;

    public function category(){
    	return $this->belongsTo('App\Models\Category','categID');
    }
    public function user(){
    	return $this->belongsTo('App\User','accountID');
    }
    public function transacts(){
        return $this->hasMany('App\Models\Transactions','productID');
    }
}