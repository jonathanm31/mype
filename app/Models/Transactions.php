<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Transactions extends Model {
    protected $table = 'Transactions';
    protected $primaryKey = 'transacID'; 
    protected $fillable = ['accountID','productID','quantity','state'];


    public function products(){
    	return $this->belongsTo('App\Models\Products','productID');
    }

    public function user(){
    	return $this->belongsTo('App\User','accountID','id');
    }
}