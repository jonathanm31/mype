<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CajaD extends Model {
    protected $table = 'cajad';
    protected $primaryKey = 'cajadID'; 
    public $timestamps = false;

    public function producto(){
    	return $this->hasOne('App\Models\Products','productID');
    }

    public function cabecera(){
    	return $this->belongsTo('App\Models\CajaH','cajahID');
    }
}