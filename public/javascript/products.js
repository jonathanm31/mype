var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
var table;
 $.get( currentLocation+'products/', function( data ) {
 	datos = JSON.parse(data);
 	console.log(data);
 	var content ='';
 	for (var i = 0; i < datos.products.length; i++) {
 			if(datos.products[i].state === 1){
 				content += '<tr><td style="background: rgba(63,213,192, 0.2);"><i class="fa fa-check-square-o "></i> <small>Vigente</small></td>';
 			}else{
 				content += '<tr ><td style="background: rgba(116,127,140, 0.1);"><i class="fa fa-minus-square-o"></i><small> Inactivo</small></td>';
 			}
 		    content += '<td>'+datos.products[i].name.toUpperCase()+'</td>';
 		    if(datos.categorys[i] !== null){
				content += '<td>'+datos.categorys[i].description.toUpperCase()+'</td>';
 		    }else{
 		    	content += '<td></td>';
 		    }
 		 		
 			content += '<td>S/. '+datos.products[i].price+'</td>';
 			content += '<td>'+datos.products[i].quantityTotal+'</td>';
 			content += '<td>'
 			content += '<button class="btn btn-success btn-xs" id="ficha" data-product="'+datos.products[i].productID+'"><i class="fa fa-file-text"></i>&nbsp Ficha</button>';  
 			if(datos.products[i].state === 1){
	 			content += '<a class="btn btn-primary btn-xs" href="ins?id='+datos.products[i].productID+'">Entrada</a>';
	 			if(datos.products[i].quantityTotal > 0 ){
	 				content += '<a class="btn red btn-xs" href="outs?id='+datos.products[i].productID+'">Salida</a> ';
	 			} 		
 		    }	
 			content += '</td></tr>'
 		}
  	
 	$('#products').append(content);
 	table = $('#products_table').DataTable( {
		  "autoWidth": true
		} );
 });

$('#products_table').on('click', '#ficha', function(event) {
	event.preventDefault();
	id = $(this).data('product');
 	window.location.replace(currentLocation+'single_product?id='+id);
});
