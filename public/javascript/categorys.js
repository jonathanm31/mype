var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
 $.get( currentLocation+'categorys/', function( cate ) {
 		var array = []
 		$.each(cate, function(index, val) {
 			 var objJS = new Object(); 
 			 objJS.id = val.id;
 			 if(val.parent==null){
 			 	objJS.parent = "#";
 			 }else{
 			 	objJS.parent = val.parent
 			 }
			 objJS.text = val.text;
			 objJS.icon = "jstree-icon jstree-themeicon fa fa-folder icon-state-warning icon-lg jstree-themeicon-custom";
			 array.push(objJS);
 		});
     $('#tree_2').jstree({
                'core': {
                    'data': array 
                }
     });
 });
$("#tree_2").on("select_node.jstree",
     function(evt, data){
     	$.get( currentLocation+'categorysID?description='+data['selected'][0], function( data ) {
     		$('#description').val(data.description);
     		$('#description').prop("disabled", false); 
     		$('#categID').val(data.categID);
     		$('#padre').val(data.parentID);
     	});
});
$('#guardar').click(function(event) {
  categID = $('#categID').val();
  description = $('#description').val();
  padre = null;


  if($('#padre').val() !== 0){
  	padre = $('#padre').val();
  }

  if(padre == categID  ){
  	toastr["info"]("Escoger otra categoria padre.");
  	return ;
  }

  if(categID !== ''){
  	if(description !== ''){
  			$.post(currentLocation+'category_update', {description: description, id: categID , parent:padre}, function(data, textStatus, xhr) {
		        window.location.replace(currentLocation+'list_categorys');
		        $('#categID').val('');
		      }).error(function(jqXHR, textStatus, errorThrown) {
		      	console.log(jqXHR);
		      	  if (jqXHR.status === 409) {
				   toastr["warning"]("Conflicto de Categorias!, prueba ingresando una categoria padre que no sea hijo de la primera.");
				  }else{
				  	 toastr["error"]("No se logro guardar la categoria, intentelo mas tarde");
				  }
		       
		      });
  	}else{
  		toastr["warning"]("Debes escribir la descripcion de la categoria.");
  		return;
  	}
  
  }else{
  	if(description !== ''){
	  		$.post(currentLocation+'category_store', {description: description, id: categID , parent:padre}, function(data, textStatus, xhr) {
	        window.location.replace(currentLocation+'list_categorys');
	        $('#categID').val('');
	      }).error(function() {
	        toastr["error"]("No se logro guardar el producto, intentelo mas tarde")
	      });
	  }else{
	  	toastr["warning"]("Debes escribir la descripcion de la categoria.");
  		return;
	  }
  }

  
});

$('#eliminar').click(function(event) {
	 categID = $('#categID').val();
	  if(categID !== ''){
			$.post(currentLocation+'category_delete', {id: categID}, function(data, textStatus, xhr) {
		        window.location.replace(currentLocation+'list_categorys');
		        $('#categID').val('');
		      }).error(function() {
		        toastr["error"]("No se logro guardar el eliminar, Puede que la categoria este siendo usada.")
		      });
	  }else{
	  		 toastr["warning"]("Escoger categoria.")
	  }

});

