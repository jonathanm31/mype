var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';
   
    function calcular_totales(ok){
      if(ok== true){
        total_detalle = 0;
        $("#caja_detalle .detalle_tr").each(function(){
            var importe = $(this).find('.detalle_importe');
            total_detalle += parseFloat(importe.html());
          
        });

       $('#total_detalle_t').text(total_detalle.toFixed(2));
       $('#total_detalle_t_i').val(total_detalle.toFixed(2));

       $('#stotal_detalle_t').text((total_detalle-(total_detalle*0.18)).toFixed(2));
       $('#stotal_detalle_t_i').val((total_detalle-(total_detalle*0.18)).toFixed(2));

       $('#igv_t').text((total_detalle*0.18).toFixed(2));
       $('#igv_t_i').val((total_detalle*0.18).toFixed(2));
     }else{
      $('#total_detalle_t').text('Error');
       $('#total_detalle_t_i').val('');

       $('#stotal_detalle_t').text('Error');
       $('#stotal_detalle_t_i').val('');

       $('#igv_t').text('Error');
       $('#igv_t_i').val('');
     }
        


    }

    $('#search_prod').keyup(function(event) {

        if($('#search_prod').val().length >4){
            query = $('#search_prod').val();
            $.get(currentLocation+"product_find?nom="+query+"", function( data ) {
                $('#search_results').html('');
                console.log(data);
                  $.each(data, function(index, value) {
                    if(value.quantityTotal > 0){
                        $('#search_results').append("<div id='item_to_add' class='list-group-item' name='"
                          +value.productID+"'>"+value.name+'</div>');
                    }
                  });
               });
        }else{
            $('#search_results').html('');
        }
    });

    $('#item_to_add').hover(function() {
            $( this ).toggleClass( "active" );
          }, function() {
            $( this ).removeClass( "active" );
          }
        );

    $( "#search_results" ).on( "click","#item_to_add", function() {

        $.get(currentLocation+"product_get?id="+$(this).attr('name')+"", function( data ) {
           //var producto = JSON.parse( data );
           console.log(data);
           var content = '<tr class="detalle_tr">';
           content += '<td class="detalle_id">'+data.product.name+'<input type="hidden" id ="productID" value="'+data.product.productID+'"></td>';
           content += '<td class="detalle_stock">'+data.product.quantityTotal+'</td>';
           content += '<td class="detalle_td_cantidad"><input type="num" id="cantidad" value="0"></td>';
           content += '<td class="detalle_td_precio">'+data.product.price+'</td>';
           content += '<td class="detalle_importe"><input type="hidden"  id="input-importe" value="0">'+0+'</td>';
           content += '<td class="delete_detalle"><a class="btn btn-danger" href="#" role="button">Quitar</a></td><tr>';
           $('#caja_detalle').append(content);
          
        });
        $('#search_results').html('');
        $('#search_prod').val('');

    });
    $('#caja_detalle').on('change', '.detalle_tr #cantidad', function(event) {
      console.log($(this).val());
      var ok = true;
      var tr = $(this).parent().parent();
      var cantidad = $(this).val();
      var stock = $(this).parent().parent().find( ".detalle_stock" ).html();

      if(cantidad > parseFloat(stock)){
        ok = false;
        tr.addClass('font-red-thunderbird');
        toastr["error"]("La cantidad de venta no puede ser mayor al stock del producto.");

      }else{
        tr.removeClass('font-red-thunderbird');
        var input_importe = $(this).parent().parent().find( ".detalle_importe #input-importe" );
        var importe = $(this).parent().parent().find( ".detalle_importe" ).html();
        var precio = $(this).parent().parent().find( ".detalle_td_precio" ).html();
        var t = (parseFloat(cantidad) * parseFloat(precio)).toFixed(2);
        $(this).parent().parent().find( ".detalle_importe" ).html(t);
        input_importe.val(t);

      }
      calcular_totales(ok);     

    });

    $( "#caja_detalle" ).on( "click",".delete_detalle", function() {
        $(this).closest('tr').remove()
        calcular_totales();
    });

    $('#guardar').click(function(event) {
    /*  var products = [];
      var quantitys = [];

      $("#caja_detalle .detalle_tr").each(function(){
            products.push($(this).find('.detalle_id').children('#productID').val());
            quantitys.push($(this).find('.detalle_td_cantidad').children('#cantidad').val());    
        });
     var totalH = $('#total_detalle_t_i').val();*/
    var d = new Date();
    var n = d.toLocaleDateString();

     $('#cliente').html($('#clientData').val());
     $('#fecha').html(n);
     var ctipo = "";
     var img = "";
     var tipo = $('#pago').val();
     switch(tipo){
      case '1' :
        ctipo += "Visa";
        img += ' <img width="50" class="" src="'+currentLocation+'images/visa.png">';
        break;
      case '2' :
        ctipo += 'Mastercard';
        img += ' <img width="50" class="" src="'+currentLocation+'images/master.jpg">';
        break;
      case '3' :
       ctipo += 'Efectivo';
       img += ' <img width="50" class="" src="'+currentLocation+'images/cash.png">';
        break;
     }
     $('#tipo').html(ctipo);
     $('#img').html(img);

    var detalle = "";
    $("#caja_detalle .detalle_tr").each(function(){   
            detalle += "<tr>";
            detalle += "<td>"+ $(this).find('.detalle_id').html()+"</td>";
            detalle += "<td>"+ $(this).find('.detalle_td_cantidad #cantidad').val()+"</td>";
            detalle += "<td>"+ $(this).find('.detalle_td_precio').html()+"</td>";
            detalle += "<td>"+ $(this).find('.detalle_importe').html()+"</td>";
            detalle += "</tr>";
            
        });
    
    $('#conf-bol').html(detalle);
    $('#total').html($('#total_detalle_t_i').val());
    $('#igv').html($('#igv_t_i').val());
    $('#stotal').html($('#stotal_detalle_t_i').val());





     var modal = $('#myModal');
     $('#myModal').modal('show');


    });
    function imprSelec(nombre) {
    var ficha = document.getElementById(nombre);
    var ventimp = window.open(' ', 'popimpr');
    ventimp.document.write( ficha.innerHTML );
    ventimp.document.close();
    ventimp.print( );
    ventimp.close();
  }